﻿using UnityEngine;
using System.Collections;
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Net.Sockets;  
using System.Text;  
using System.Threading;  
using System.Net;  
using System.IO;
namespace RemoteDebug.Net {  
	
	public class Client  {
		Socket socket;
		public bool isCutdown = false;
		TCPServer server;

		private string _clientName;

	

		private  byte[] result = new byte[1024];  

		public string ClientName {
			get {
				return _clientName;
			}
		}

		public Client(Socket _socket, TCPServer _server){
			socket = _socket;
			server = _server;

		}

		public void Start(){
			_clientName = socket.RemoteEndPoint.ToString ();
			Debug.Log ("客户端{0}成功连接:" + socket.RemoteEndPoint.ToString ());
			//向连接的客户端发送连接成功的数据  
//			SendMessage ("Connected Server");
			Thread thread = new Thread(RecieveMessage);
			thread.Start(socket);  
		}

		public bool isConnected(){
			return !isCutdown;
		}

		public void SendMessage(string str){
			if (!isConnected ())
				return;
			byte[] data = Encoding.UTF8.GetBytes (str);
			socket.Send(data);  
		}

		public void Disconnect(bool isRemove = true)
		{
			if (isCutdown)
				return;
			Debug.Log ("Disconnect");
//			
			socket.Close();
			isCutdown = true;
		}

		void copBity(byte[] src, byte[] des, int start){
			int count = src.Length;
			for (int i = 0; i < count; i++) {
				des [start + i] = src [i];
			}
		}

		private  void RecieveMessage(object obj)  
		{  
			if (!isConnected ())
				return;
			Socket mClientSocket = (Socket)obj;
			bool flag = true;
			string content = "";
			while (flag)  
			{  
				try{
					int receiveNumber = mClientSocket.Receive(result);
					if (receiveNumber>0){
						
						content += Encoding.UTF8.GetString(result, 0, receiveNumber);
						if (receiveNumber < 1024){
							server.addReceive(content); 
							content = "";
						}
					}else{
						flag = false;
						socket.Shutdown(SocketShutdown.Both);
//						Disconnect (true);
					}
				}
				catch(Exception e){
					flag = false;
					Debug.Log (e.Message);
					Disconnect (true);
				}
			}  
		}

		/// <summary>  
		/// 数据转换，网络发送需要两部分数据，一是数据长度，二是主体数据  
		/// </summary>  
		/// <param name="message"></param>  
		/// <returns></returns>  
		private  byte[] WriteMessage(byte[] message)  
		{  
			MemoryStream ms = null;  
			using (ms = new MemoryStream())  
			{  
				ms.Position = 0;  
				BinaryWriter writer = new BinaryWriter(ms);  
				ushort msglen = (ushort)message.Length;  
//				writer.Write(msglen);  
				writer.Write(message);  
				writer.Flush();  
				return ms.ToArray();  
			}  
		}  

	}

}
