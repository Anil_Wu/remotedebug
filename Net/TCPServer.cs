﻿using UnityEngine;
using System.Collections;
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Net.Sockets;  
using System.Text;  
using System.Threading;  
using System.Net;  
using System.IO;
namespace  RemoteDebug.Net { 
	public delegate void Receive (string s);

	public class TCPServer  {
		public const int defaultPort = 8088;
		public Receive onReceive = null;
		public bool isStart = false;
		private  Socket serverSocket;  
		public int maxLinks = 10;

		private string iP;
		private int port;

		List<string> receiveDatas;
		List<string> sendDatas;
		List<Client> clients;

		public string IP {
			get {
				return iP;
			}
		}

		public int Port {
			get {
				return port;
			}
		}

		public List<string> ReceiveDatas {
			get {
				return receiveDatas;
			}
		}

		public TCPServer(){
			receiveDatas = new List<string> ();
			sendDatas = new List<string> ();
			clients = new List<Client> ();
		}

		public void Start (string _ip , int _port) {
			doStart (_ip,_port);
		}

		public void Start(){
			doStart ("127.0.0.1",defaultPort);
		}
			
		public void Stop(){
			if (!isStart)
				return;
			isStart = false;
			lock (clients) {
				foreach(Client c in clients){
					c.Disconnect (false);
				}
				clients.Clear ();
			}

			if (serverSocket != null) {
				serverSocket.Close ();
			}
		}

		public int getLinks(){
			if (!isStart)
				return 0;
			lock (clients) {
				return clients.Count;
			}
		}
			
		public void addClient(Client newC){
			lock (clients) {
				clients.Add (newC);
			}
		}

		public void removeClient(Client c){
//			lock (clients) {
//				Debug.Log ("REMOVE " + c.ClientName);
				clients.Remove (c);
//			}
		}

		public void addReceive(string str){
			if (!isStart)
				return;
			lock(receiveDatas){
				receiveDatas.Add(str);
			}
		}

		// 在主线程里面调用这个方法
		public void readReceive(){
			if (!isStart)
				return;
			lock(receiveDatas){
				if (onReceive != null) {
					foreach( string str in receiveDatas ){
						onReceive (str);
					}
				}
				receiveDatas.Clear ();

				lock (clients) {
					List<Client> list = new List<Client>();
					foreach (Client c in clients) {
						if (!c.isConnected ()) {
							list.Add (c);
						}
					}

					if (list.Count>0) {
						foreach (Client c in list) {
							removeClient (c);
							string jsonStr = "{\"command\":\"disconnect\",\"data\":{\"content\":\"" + c.ClientName +  "\"}}";
							receiveDatas.Add (jsonStr);
						}
					}

					list.Clear ();
				}
			}
		}

		public void send(string str){
			if (!isStart)
				return;
			lock (sendDatas) {
				sendDatas.Add (str);
			}
		}

		private void doStart (string _ip , int _port ) {
			if (isStart)
				return;
			
			iP = _ip;
			port = _port;
			isStart = true;
			IPAddress ipAddress = IPAddress.Parse(iP);  
			IPEndPoint ip_end_point = new IPEndPoint(ipAddress, port);  
			//创建服务器Socket对象，并设置相关属性  
			serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);  
			//绑定ip和端口  
			serverSocket.Bind(ip_end_point);  
			//设置最长的连接请求队列长度  
			serverSocket.Listen(maxLinks);  
			//在新线程中监听客户端的连接  
			Thread thread = new Thread(clientConnectListen);  
			thread.Start();

			//消息广播线程
			Thread thread2 = new Thread(broadcast);  
			thread2.Start();  
		}

		private void clientConnectListen()  
		{  
			while (true)  
			{  
				Socket clientSocket = serverSocket.Accept();  
				Client c = new Client (clientSocket, this);
				addClient (c);
				c.Start ();

				lock (receiveDatas) {
					string jsonStr = "{\"command\":\"connect\",\"data\":{\"content\":\"" + c.ClientName + "\"}}";
					receiveDatas.Add (jsonStr);

				}
			}  
		}  

		private  void broadcast()  
		{  
			while(true){
				lock (sendDatas) {
					lock (clients) {
						foreach (string str in sendDatas) {
							foreach (Client c  in clients) {
								c.SendMessage(str);  
							}
						}
					}
					sendDatas.Clear ();
				}
			}

		}  
	}   

}
