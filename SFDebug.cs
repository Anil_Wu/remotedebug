﻿using RemoteDebug.Net;	
using System.Net;
using UnityEngine;
//using SLua;


namespace RemoteDebug{

//	[CustomLuaClassAttribute]
	public class SFDebug  {
		static SFDebug debug = null;
		TCPServer server;

		SFDebug(Receive _onReceive){
			server = new TCPServer ();
			server.onReceive = _onReceive;
		}

		public static void Init(Receive _onReceived){
			if (debug == null) {
				debug = new SFDebug (_onReceived);
			}
		}

		public static void Start(bool islocal=true){
			if (islocal) {
				debug.server.Start ();
			} else {
				string ip = getIp ();
				if (ip == "") {
					debug.server.Start ();
				} else {
					
					debug.server.Start (ip, TCPServer.defaultPort);
				}
			}
		}

		public static void Start(string ip, int port){
			debug.server.Start (ip, port);
		}

		public static void Stop(){
			debug.server.Stop();
		}

		public static bool IsStart(){
			return debug.server.isStart;
		}


		public static int getLinkNum(){
			return debug.server.getLinks ();
		}
		public static void Send(string json){
			debug.server.send (json);
		}

		public static void ReadReceive(){
			if (debug == null)
				return;
			
			debug.server.readReceive ();
		}

		public static string GetServerIP(){
			if (!debug.server.isStart) {
				return "";
			}
			return debug.server.IP;
		}

		public static int GetServerPort(){
			if (!debug.server.isStart) {
				return -1;
			}
			return debug.server.Port;
		}


		public static void Log(LogType type, string content){
			if (debug == null)
				return;
			string tag = getLogTag (type);
			content = content.Replace ("\"", "\\\"");
			content = content.Replace ("\r", "\\r");
			content = content.Replace ("\n", "\\n");
			content = content.Replace ("\r\n", "\\r\\n");

			string json = "{\"command\":\"log_output\",\"data\":{\"tag\":\"" + tag + "\",\"log\":\"" + content +"\"}}\n";
			debug.server.send (json);
		}

		static string getLogTag(LogType type){
			switch (type)
			{
			case LogType.Assert:
				return "Assert";
			case LogType.Error:
				return "Error";
			case LogType.Exception:
				return "Exception";
			case LogType.Log:
				return "Log";
			case LogType.Warning:
				return "Warning";

			}

			return "";
		}

		static string getIp(){
			IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
			IPAddress[] addressList = ipHost.AddressList;

			foreach (IPAddress ipAddr in addressList) {
				string ip = ipAddr.ToString();
				Debug.Log (ip);
				if (isIpV4 (ip)) {
					return ip;
				}
			}

			return "";
		}

		static bool isIpV4(string str){
			string[] strs = str.Split('.');

			if (strs.Length == 4) {
				return true;
			}

			return false;
		}
	}
}
	